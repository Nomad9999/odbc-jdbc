package app.gui;

import app.database.DatabaseConnection;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.sql.*;
import java.util.Objects;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


@SuppressWarnings("Duplicates")
public class MainFrame extends JFrame {
    private JTable dataTable;
    private JScrollPane dataPanel;
    private JTextField queryField = new JTextField();
    private JButton executeButton = new JButton("Wykonaj");
    private String query = "";


    public MainFrame() {
        initWindow();
        createTable();
        setVisible(true);
    }

    private void initWindow() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(1000, 750);
        setLocation(200, 0);
        setLayout(new BorderLayout());
    }

    private void createTable() {
        dataTable = new JTable();
        dataPanel = new JScrollPane(dataTable);
        add(dataPanel, BorderLayout.CENTER);
        add(queryField, BorderLayout.NORTH);
        add(executeButton, BorderLayout.SOUTH);
        executeButton.addActionListener(new ActionListener() {
            @Override
            public  void actionPerformed(ActionEvent e) {
                getResults();
            }
        });

    }

    private void getResults() {
        synchronized (this) {
            try {
                Connection con = DatabaseConnection.getConnection();
                Statement statement = con
                        .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                query = queryField.getText();
                ResultSet resultSet = statement.executeQuery(query);

                resultSet.last();
                int rowCount = resultSet.getRow();
                resultSet.beforeFirst();

                ResultSetMetaData metaData = resultSet.getMetaData();
                int columnCount = metaData.getColumnCount();
                Object[] columnNames = new Object[columnCount];

                for (int i = 0; i < columnCount; i++) {
                    columnNames[i] = metaData.getCatalogName(i + 1);
                }

                Object[][] data = new Object[rowCount][columnCount];
                int i = 0;
                while (resultSet.next()) {
                    for (int j = 0; j < columnCount; j++) {
                        data[i][j] = resultSet.getString(j + 1);
                    }
                    i++;
                }

                DefaultTableModel defaultTableModel = new DefaultTableModel(data, columnNames);
                dataTable.setModel(defaultTableModel);

                con.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }
}
