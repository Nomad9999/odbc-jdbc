package app.database;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static app.database.DatabaseConnection.getConnection;

@SuppressWarnings("ALL")
public class Repository {

    public static long insertPriceList(String description, String date) {
        Connection connection = getConnection();
        String sql = "INSERT INTO PRICE_LIST(description, price_list_date) VALUES (?, ?)";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, description);
            try {
                statement.setDate(2, new Date(simpleDateFormat.parse(date).getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            } statement.execute();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            return generatedKeys.getLong(1);
        } catch (SQLException e) {
            System.out.println("Error occurred while inserting price list to database");
            e.printStackTrace();
        }

        return -1;
    }

    public static long insertGroup(String groupName, String shortcut, long priceListId) {
        Connection connection = getConnection();
        String sql = "INSERT INTO GROUPS(group_name, shortcut, price_list_id) VALUES (?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, groupName);
            statement.setString(2, shortcut);
            statement.setLong(3, priceListId);
            statement.execute();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            return generatedKeys.getLong(1);
        } catch (SQLException e) {
            System.out.println("Error occurred while inserting group to database");
            e.printStackTrace();
        }

        return -1;
    }

    public static long insertProduct(long groupId) {
        Connection connection = getConnection();
        String sql = "INSERT INTO PRODUCTS(group_id) VALUES (?)";

        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setLong(1, groupId);
            statement.execute();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            return generatedKeys.getLong(1);
        } catch (SQLException e) {
            System.out.println("Error occurred while inserting product to database");
            e.printStackTrace();
        }

        return -1;
    }

    public static long insertAttribute(String attributeName, String attributeValue, long productId) {
        Connection connection = getConnection();
        String sql = "INSERT INTO ATTRIBUTES(attribute_name, attribute_value, product_id) VALUES (?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, attributeName);
            statement.setString(2, attributeValue);
            statement.setLong(3, productId);
            statement.execute();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            return generatedKeys.getLong(1);
        } catch (SQLException e) {
            System.out.println("Error occurred while inserting attribute to database");
            e.printStackTrace();
        }

        return -1;
    }
}
