package app.database;


import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@SuppressWarnings("ALL")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DatabaseConnection {
    static private Connection connection;

    public static Connection getConnection() {
        try {
            if (connection == null || connection.isClosed())
                connect();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    private static void connect() {
        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            //Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:odbc:PostgreSQL35W", "ikms", "ikms");
            //connection = DriverManager.getConnection("jdbc:postgresql://192.168.99.1:5432/integracja", "ikms", "ikms");
            System.out.println("Established connection to database");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Could not establish connection with database");
            throw new RuntimeException(e);
        }
    }

    public static void createGroupsTable() {
        Connection connection = DatabaseConnection.getConnection();

        System.out.println("Creating GROUP table...");
        try (Statement statement = connection.createStatement()) {
            String sql = "CREATE TABLE IF NOT EXISTS GROUPS " +
                    "(id SERIAL PRIMARY KEY not NULL, " +
                    "group_name VARCHAR(255), " +
                    "shortcut VARCHAR(10), " +
                    "price_list_id INTEGER REFERENCES price_list(id))";
            statement.execute(sql);

            System.out.println("GROUP table was created");
        } catch (SQLException e) {
            System.out.println("Error during creation of GROUP table");
            e.printStackTrace();
        }
    }

    public static void createPriceListTable() {
        Connection connection = DatabaseConnection.getConnection();

        System.out.println("Creating PRICE_LIST table...");
        try (Statement statement = connection.createStatement()) {
            String sql = "CREATE TABLE IF NOT EXISTS PRICE_LIST " +
                    "(id SERIAL PRIMARY KEY not NULL, " +
                    "description VARCHAR(255), " +
                    "price_list_date DATE)";
            statement.execute(sql);

            System.out.println("PRICE_LIST table was created");
        } catch (SQLException e) {
            System.out.println("Error during creation of PRICE_LIST table");
            e.printStackTrace();
        }
    }

    public static void createProductTable() {
        Connection connection = DatabaseConnection.getConnection();

        System.out.println("Creating PRODUCTS table...");
        try (Statement statement = connection.createStatement()) {
            String sql = "CREATE TABLE IF NOT EXISTS PRODUCTS " +
                    "(id SERIAL PRIMARY KEY not NULL, " +
                    "group_id INTEGER REFERENCES groups(id))";
            statement.execute(sql);

            System.out.println("PRODUCTS table was created");
        } catch (SQLException e) {
            System.out.println("Error during creation of PRODUCTS table");
            e.printStackTrace();
        }
    }

    public static void createAttributesTable() {
        Connection connection = DatabaseConnection.getConnection();

        System.out.println("Creating ATTRIBUTES table...");
        try (Statement statement = connection.createStatement()) {
            String sql = "CREATE TABLE IF NOT EXISTS ATTRIBUTES " +
                    "(id SERIAL PRIMARY KEY not NULL, " +
                    "attribute_name VARCHAR(255), " +
                    "attribute_value VARCHAR(255), " +
                    "product_id INTEGER REFERENCES products(id))";
            statement.execute(sql);

            System.out.println("ATTRIBUTES table was created");
        } catch (SQLException e) {
            System.out.println("Error during creation of ATTRIBUTES table");
            e.printStackTrace();
        }
    }
}
