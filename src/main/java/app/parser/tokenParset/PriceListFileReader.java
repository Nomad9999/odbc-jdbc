package app.parser.tokenParset;


import app.database.Repository;
import app.parser.tokenParset.concreteStates.GraphicCardState;
import app.parser.tokenParset.concreteStates.MemoryState;
import app.parser.tokenParset.concreteStates.NormalLineState;
import app.parser.tokenParset.concreteStates.ProcessorState;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.StringTokenizer;

public class PriceListFileReader {

    private final static String FILE_PATH = "data.txt";
    private final static String DELIMITER = "|";

    private TokenParserState parserState = new NormalLineState();

    private long currentGroupId = -1;

    public void loadToDatabaseFromFile() {
        URL dataFileUrl = getClass().getClassLoader().getResource(FILE_PATH);
        Objects.requireNonNull(dataFileUrl, "Could not find file " + FILE_PATH);

        File file = new File(dataFileUrl.getFile());
        try (FileReader fileReader = new FileReader(file); BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            String line;
            long priceListId = insertPriceList();
            while ((line = bufferedReader.readLine()) != null) {
                StringTokenizer stringTokenizer = new StringTokenizer(line);
                if (stringTokenizer.hasMoreElements()) {
                    String rowType = stringTokenizer.nextToken(DELIMITER);
                    if (shouldChangeState(rowType)) {
                        changeStateByRowType(stringTokenizer.nextToken(DELIMITER));
                        currentGroupId = createGroup(priceListId);
                        printGroupName();
                        continue;
                    }
                    if (isProduct(rowType))
                        insertProduct(stringTokenizer);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private long createGroup(long priceListId) {
        return Repository.insertGroup(parserState.getGroupName(), parserState.getGroupShortName(), priceListId);
    }

    private long insertPriceList() {
        return Repository.insertPriceList("ItemListFromFile", "2018-11-11");
    }

    private boolean isProduct(String rowType) {
        return "PRD".equals(rowType);
    }

    private boolean shouldChangeState(String rowType) {
        return "GRP".equals(rowType);
    }

    private void changeStateByRowType(String rowType) {
        if ("PROC".equals(rowType)) {
            this.parserState = new ProcessorState();
            return;
        }
        if ("GRAPH".equals(rowType)) {
            this.parserState = new GraphicCardState();
            return;
        }
        if ("MEM".equals(rowType)) {
            this.parserState = new MemoryState();
            return;
        }

        this.parserState = new NormalLineState();
    }

    private void printGroupName() {
        this.parserState.printGroupName();
    }

    private void insertProduct(StringTokenizer restOfLine) {
        this.parserState.insertProduct(restOfLine, currentGroupId);
    }
}
