package app.parser.tokenParset;

import java.util.StringTokenizer;

public abstract class TokenParserState {

    public abstract void insertProduct(StringTokenizer line, long groupId);

    public abstract void printGroupName();

    public abstract String getGroupName();

    public abstract String getGroupShortName();
}
