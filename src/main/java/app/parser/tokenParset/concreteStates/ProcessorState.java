package app.parser.tokenParset.concreteStates;

import app.database.Repository;
import app.parser.tokenParset.TokenParserState;

import java.util.StringTokenizer;


public class ProcessorState extends TokenParserState {

    private final static String[] COL_NAMES = {"Producent: ", "Model: ", "Rdzenie: ", "Symbol: ", "Czestotliwosc: ", "L1: ", "L2: ", "L3: ", "Cena: ", "Opakowanie: "};

    @Override
    public void insertProduct(StringTokenizer restOfRow, long groupId) {
        long productId = Repository.insertProduct(groupId);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < COL_NAMES.length; i++) {
            if (restOfRow.hasMoreElements()){
                String nextToken = restOfRow.nextToken("|");
                Repository.insertAttribute(COL_NAMES[i], nextToken, productId);
                sb.append(COL_NAMES[i]).append(" ".equals(nextToken) ? "brak" : nextToken).append("\t");
            }
        }

        System.out.println(sb.toString());
    }

    @Override
    public void printGroupName() {
        System.out.println("\nProcesory:");
    }

    @Override
    public String getGroupName() {
        return "Procesory";
    }

    @Override
    public String getGroupShortName() {
        return "PROC";
    }

}
