package app.parser.tokenParset.concreteStates;

import app.parser.tokenParset.TokenParserState;

import java.util.StringTokenizer;

public class NormalLineState extends TokenParserState {
    @Override
    public void insertProduct(StringTokenizer restOfRow, long groupId) {
        System.out.println(restOfRow.nextToken(""));
    }

    @Override
    public void printGroupName() {
        System.out.println();
    }

    @Override
    public String getGroupName() {
        return "";
    }

    @Override
    public String getGroupShortName() {
        return "";
    }
}
