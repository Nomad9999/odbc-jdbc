package app.parser.tokenParset.concreteStates;

import app.database.Repository;
import app.parser.tokenParset.TokenParserState;

import java.util.StringTokenizer;

public class MemoryState extends TokenParserState {

    private final static String[] COL_NAMES = {"Producent: ", "Model: ", "Rodzaj pamieci: ", "Pamiec: ", "Taktowanie: ", "Napiecie: "};

    @Override
    public void insertProduct(StringTokenizer restOfRow, long groupId) {
        long productId = Repository.insertProduct(groupId);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < COL_NAMES.length; i++) {
            if (restOfRow.hasMoreElements()){
                String nextToken = restOfRow.nextToken("|");
                Repository.insertAttribute(COL_NAMES[i], nextToken, productId);
                sb.append(COL_NAMES[i]).append(" ".equals(nextToken) ? "brak" : nextToken).append("\t");
            }
        }

        System.out.println(sb.toString());
    }

    @Override
    public void printGroupName() {
        System.out.println("\nPamięć RAM:");
    }

    @Override
    public String getGroupName() {
        return "Pamiec RAM";
    }

    @Override
    public String getGroupShortName() {
        return "MEM";
    }
}
