package app.parser.tokenParset.concreteStates;

import app.database.Repository;
import app.parser.tokenParset.TokenParserState;

import java.util.StringTokenizer;

public class GraphicCardState extends TokenParserState {

    private final static String[] COL_NAMES = {"Producent: ", "Model: ", "Ilosc pamieci: ", "Rodzaj pamieci: ",   "Taktowanie pamieci: ", "Taktowanie rdzenia: "};

    @Override
    public void insertProduct(StringTokenizer restOfRow, long groupId) {
        long productId = Repository.insertProduct(groupId);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < COL_NAMES.length; i++) {
            if (restOfRow.hasMoreElements()){
                String nextToken = restOfRow.nextToken("|");
                Repository.insertAttribute(COL_NAMES[i], nextToken, productId);
                sb.append(COL_NAMES[i]).append(" ".equals(nextToken) ? "brak" : nextToken).append("\t");
            }
        }

        System.out.println(sb.toString());
    }

    @Override
    public void printGroupName() {
        System.out.println("\nKarty Graficzne:");
    }

    @Override
    public String getGroupName() {
        return "Karty graficzne";
    }

    @Override
    public String getGroupShortName() {
        return "GRAPH";
    }
}
