package app;

import app.database.DatabaseConnection;
import app.gui.MainFrame;
import app.parser.tokenParset.PriceListFileReader;

public class Main {
    public static void main(String[] args) {
        //DatabaseConnection.getConnection();
//        DatabaseConnection.createPriceListTable();
//        DatabaseConnection.createGroupsTable();
//        DatabaseConnection.createProductTable();
//        DatabaseConnection.createAttributesTable();
//        new PriceListFileReader().loadToDatabaseFromFile();
        new MainFrame();
    }
}
